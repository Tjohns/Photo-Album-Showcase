﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Photo_Album
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private string url = "https://jsonplaceholder.typicode.com/photos";
        private HttpClient _Client = new HttpClient();
        private ObservableCollection<Photos> _photos;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void Page_LoadedAsync(object sender, RoutedEventArgs e)
        {
            var content = await _Client.GetStringAsync(url);
            var photos = JsonConvert.DeserializeObject<List<Photos>>(content);
            _photos = new ObservableCollection<Photos>(photos);
            PhotosListView.ItemsSource = _photos;
        }
    }

    public class Photos
    {
        public int albumId { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string thumbnailUrl { get; set; }

        public object albumIdTitleAndId
        {
            get
            {
                return "> albumId: " + albumId + "\n"
                    + "[" + id + "] - " + title;
            }
        }
    }
}
